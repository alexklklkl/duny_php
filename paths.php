<?
define("DS", DIRECTORY_SEPARATOR);
define("ROOT", getcwd() . DS);

define("APP_PATH", ROOT . 'application' . DS);
define("FRAMEWORK_PATH", ROOT . "framework" . DS);
define("PUBLIC_PATH", ROOT . "public" . DS);
define("CONFIG_PATH", APP_PATH . "config" . DS);

define("CONTROLLER_PATH", APP_PATH . "controllers" . DS);
define("MODEL_PATH", APP_PATH . "models" . DS);
define("VIEW_PATH", APP_PATH . "views" . DS);
define("TEMPLATE_PATH", VIEW_PATH . "templates" . DS);

define("CORE_PATH", FRAMEWORK_PATH . "core" . DS);
define("DB_PATH", FRAMEWORK_PATH . "database" . DS);
define("LIB_PATH", FRAMEWORK_PATH . "libraries" . DS);
define("HELPER_PATH", FRAMEWORK_PATH . "helpers" . DS);
define("LOG_PATH", FRAMEWORK_PATH . "log" . DS);
define("LOG_DEBUG_FILE", "debug.log");
define("LOG_ERROR_FILE", "error.log");

define("UPLOAD_PATH", PUBLIC_PATH . "uploads" . DS);

define("EXT", ".php");


?>