<?
	/**
	* Вспомогательные функции
	*/
	class Helper {
		
		public static function str_replace($search, $replace, $subject) {
			while (strpos($subject, $search)) {
				$subject = str_replace($search, $replace, $subject);
			}
			return $subject;
		}
		
		public static function getSafeString($str) {
	        $str = trim($str);
	        $str = stripslashes($str);
	        $str = htmlspecialchars($str);
	        return $str;
	    }
	    
	    public static function print_r_($val) {
		    echo "<pre>";
		    print_r($val);
		    echo "<pre>";
	    }
		
	}
	