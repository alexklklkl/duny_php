<?
	
	/**
	* Работа с параметрами
	*/
	class Params {
		
		protected static $params = array();
		protected static $controllerName;
		protected static $actionName;
			    	    
		public static function initParams() {			
			$params = self::prepareParamsString();
			
			// Контроллер
			$controllerName = self::parseControllerName($params);
			self::setControllerName($controllerName);
			
			// Акция
			$actionName = self::parseActionName($params);
			self::setActionName($actionName);
			
			//Параметры строки
			self::initParamsFromString($params);
			
			// Параметры GET и POST, COOKIE не здесь
			self::initParamsGet();
			self::initParamsPost();
		}

		// Если установлен параметр - вернем, иначе default, а если и его нет - ошибка
	    public static function get($paramKey, $default = NULL) {
		    if (!empty(self::$params[$paramKey])) {
				return self::$params[$paramKey];
		    } else {
			    if (!empty($default)) {
				    return $default;
			    } else {
					\Error::trow(__METHOD__." Запрошен несуществующий параметр: ".$paramKey);
				}
		    }
	    }
		
		// Контроллер - первый параметр
	    protected static function parseControllerName(&$params) {
			if (!empty($params[0])) {
				$controllerName = \Helper::getSafeString($params[0]);
				// Уберем из $params имя контроллера
				array_splice($params, 0, 1); 
			} else {
				$controllerName = CONTROLLER_DEFAULT;
			}
			return $controllerName;   
	    }
	    
		// Акция - второй параметр, но после array_splice в parseControllerName - он первый
	    protected static function parseActionName(&$params) {
			if (!empty($params[0])) {
				$actionName = \Helper::getSafeString($params[0]);
				// Уберем из $params имя акции
				array_splice($params, 0, 1); 
			} else {
				$actionName = ACTION_DEFAULT;
			}
			return $actionName;
		}
		
		//Разберем пары key => value
		protected static function initParamsFromString($params) {
			// в $params только параметры, уже нет контроллера и акции
			$params = array_chunk($params, 2);
			foreach ($params as $param) {
				// $key есть всегда
				$key = \Helper::getSafeString($param[0]);
				// $value может не быть
				if (!empty($param[1]))
					$value = \Helper::getSafeString($param[1]);
				else
					$value = "";
				self::$params[$key] = $value;
			}			
		}
		
		// Обработаем get-параметры
		protected static function initParamsGet() {
			foreach($_GET as $key=>$value) {
				self::$params[\Helper::getSafeString($key)] = \Helper::getSafeString($value);
			}
		}		
			    
		// Обработаем post-параметры
		protected static function initParamsPost() {
			foreach($_POST as $key=>$value) {
				self::$params[\Helper::getSafeString($key)] = \Helper::getSafeString($value);
			}
		}		
		
		// Почистим строку параметров перед обработкой
	    protected static function prepareParamsString() {
			$params = $_SERVER['REQUEST_URI'];
			$offset = strpos($params, "?");
			if ($offset !== false) {
				$params = substr($params, 0, $offset);
			}
			$params = trim($params, '/');
			$params = \Helper::str_replace('//', '/', $params);
			$params = explode("/", $params);
			$params = array_filter($params);
		    return $params;
	    }	    

		public static function setControllerName($controllerName) {
			self::$controllerName = $controllerName;
		}
		public static function getControllerName() {
			return self::$controllerName;
		}
		public static function setActionName($actionName) {
			self::$actionName = $actionName;
		}
		public static function getActionName() {
			return self::$actionName;
		}
			    
	}
	
	
	