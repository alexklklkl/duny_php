<?
	
	/**
	* Error
	*/
	class Error{
		public static function trow($message) {
			\Log::error($message);
			if (DEBUG) {
				throw new \Exception($message);
			} else {
				echo "Какая-то ошибка, обратитесь к администратору.";
			}
			exit;
		}
	}
	