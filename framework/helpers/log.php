<?
	class Log {
		public static function debug($message) {
			file_put_contents(LOG_PATH.LOG_DEBUG_FILE, date("d.m.Y H:i:s")." - ".$message."\n", FILE_APPEND);
		}
		public static function error($message) {
			file_put_contents(LOG_PATH.LOG_ERROR_FILE, date("d.m.Y H:i:s")." - ".$message."\n", FILE_APPEND);
		}
	}
	