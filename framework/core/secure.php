<?
namespace Framework\Core;
/**
* Безопасность и авторизация
*/
class Secure {
	
	protected static $allowOrigins = array("http://ru.grad39.ru");
	protected static $addedHeaders = array();
	
	public static function allowAccess() {
		// Разрешенный Origin и авторизованный пользователь
		return self::allowAccessOrigin() && self::allowAccessRights();
	}
	
	// Ограничение по Origin
	protected static function allowAccessOrigin() {
		$headers = getallheaders();
		if (isset($headers['Origin'])) {
			if (in_array($headers['Origin'], self::$allowOrigins)) {
				self::$addedHeaders[] = "Access-Control-Allow-Origin: ".$headers['Origin'];
				return true;			
			} else {
				return false;
			}
		} else {
			// Запрос из браузера
			return true;
		}
		return false;
	}
	
	// Авторизация пользователя
	protected static function allowAccessRights() {
		// TODO
		return true;
	}
	
	public static function getAddedHeaders() {
		return self::$addedHeaders;
	}
}
