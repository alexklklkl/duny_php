<?
	
	/**
	* Loader
	*/
	class Loader {
		public static function init($includePath) {
			set_include_path($includePath);
			spl_autoload_register(function ($class) {
				$fileName = str_replace('\\', '/', $class).EXT;
				
				// Чистка для файловой системы, пример: OrdersController.php -> orders.php
				$fileName = \Loader::clearClassName($fileName);
				
				if (file_exists($fileName)) {
					require_once $fileName; 
				} else {
					\Error::trow(__CLASS__." - файл ".$fileName." не существует");
				}
			});
			
		}
		
		// Уберем из имен файлов последние "controller" и т.д. 
		public static function clearClassName($fileName) {
			$fileName = strtolower($fileName);
			if (strpos($fileName, "/controller".EXT) === false)
				$fileName = str_replace('controller'.EXT, EXT, $fileName);
			if (strpos($fileName, "/model".EXT) === false)
				$fileName = str_replace('model'.EXT, EXT, $fileName);
			if (strpos($fileName, "/view".EXT) === false)
				$fileName = str_replace('view'.EXT, EXT, $fileName);			
			return $fileName;
		}
	}
	