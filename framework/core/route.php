<?
namespace Framework\Core;

class Route
{	
	static function start() {
		
		// Проверка доступа
		if (!\Framework\Core\Secure::allowAccess()) {
			\Error::trow("Доступ запрещен");			
		}
		
		$controllerName = \Params::getControllerName();
		$actionName = \Params::getActionName();
		
		// Создание котроллера
		$controllerClassName = ucfirst($controllerName)."Controller";
		$controllerClassName = "Application\\Controllers\\".$controllerClassName;
		if (!class_exists($controllerClassName)) {
			\Error::trow("Нет класса ".$controllerClassName);
		}
		$controller = new $controllerClassName;
		
		// Акция
		$action = $actionName;
		if (!method_exists($controller, $action)) {
			\Error::trow("В классе ".get_class($controller)." нет метода ".$action);
		}
		
		// Вызов метода
		$controller->$action();
	}
	
}