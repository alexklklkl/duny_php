<?
// Константы и классы в глобальном пространстве
require_once CONFIG_PATH.'config.php';
require_once HELPER_PATH.'log.php';
require_once HELPER_PATH.'error.php';
require_once HELPER_PATH.'helper.php';
require_once HELPER_PATH.'params.php';
require_once DB_PATH.'database.php';
require_once CORE_PATH.'loader.php';

// Инизиализация autoloader
Loader::init(ROOT);
// Парсинг параметров
Params::initParams();
// БД
DB::connect();
// Маршрутизация
Framework\Core\Route::start();
