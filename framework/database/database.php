<?

/**
* DB
*/
class DB {
	private static $DB = null;
	
	public static function connect() {
		try {
			$pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
			$pdo_options[PDO::ATTR_DEFAULT_FETCH_MODE] = PDO::FETCH_ASSOC;
			self::$DB = new PDO('mysql:host='.DB_HOST.';dbname='.DB_NAME.';charset=UTF8', DB_USER, DB_PASS, $pdo_options);
			self::$DB->exec("set names utf8");
		} catch (PDOException $e) {
			\Error::trow(__METHOD__." Не удалось подключиться к базе данных: ".$e->getMessage());			
		}
	}
	public static function query($query) {
		try {
			$result = self::$DB->query($query);
			return $result->fetchAll();
		} catch (PDOException $e) {
			\Error::trow(__METHOD__." Не удалось выполнить запрос: ".$e->getMessage());			
		}
	}
	public static function exec($query) {
		try {
			$result = self::$DB->exec($query);
			return $result;
		} catch (PDOException $e) {
			\Error::trow(__METHOD__." Не удалось выполнить запрос: ".$e->getMessage());			
		}
	}
}
