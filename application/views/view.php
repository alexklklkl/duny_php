<?
namespace Application\Views;
	
/**
*  View
*/
class View {

	function __construct() {
	}

	public function index() {
		if (DEBUG)
			echo __METHOD__."<br />";
	}
	
	// Вывод соотвествующего шаблона
	public function generate($commonTemplate, $contentTemplate, $data = array()) {
		require_once TEMPLATE_PATH.$commonTemplate;
	}	
}
	