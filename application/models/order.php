<?
namespace Application\Models;
	
/**
* Order Model
*/
class OrderModel extends Model {

	function __construct() {
		parent::__construct();
	}
	
	public function getOrdersWithStatus($statusId) {
		$query = "SELECT * FROM ".DB_PREFIX."orders where statusId = ".$statusId." ORDER BY id";
		$result = \DB::query($query);
		return $result;
	}
	
	public function getOrdersFullTextWithStatus($statusId, $data = array()) {
		extract($data);
		$query = "SELECT o.*, p.pricecategory as type, (o.countAdult + o.countChild + o.countFree) as totalPerson, os.status FROM ".DB_PREFIX."orders o inner join ".DB_PREFIX."pricecategorys p on p.pricecategoryid = o.typeId inner join ".DB_PREFIX."orderstatuses os on os.id = o.statusId where statusId = ".$statusId." ORDER BY $sidx $sord LIMIT $start, $limit";
		$result = \DB::query($query);
		return $result;
	}
	
	public function getOrderCountWithStatus($statusId) {
		$query = "SELECT COUNT(*) AS count FROM ".DB_PREFIX."orders where statusId = ".$statusId;
		$result = \DB::query($query);
		return intval($result[0]['count']);
	}
	
	public function getOrderStatusById($statusId) {
		$query = "SELECT status FROM ".DB_PREFIX."orderstatuses where id = ".$statusId;
		$result = \DB::query($query);
		return $result[0]['status'];
	}
	
	function getOrderPriceCategoryById($typeId) {
		$query = "SELECT pricecategory FROM pricecategorys where pricecategoryid = ".$typeId;
		$result = \DB::query($query);
		return $result[0]['pricecategory'];
	}
	


	
}
	