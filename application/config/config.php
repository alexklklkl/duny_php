<?
define("DEBUG", true);
define("CONTROLLER_INDEX", 0);
define("CONTROLLER_DEFAULT", "main");
define("ACTION_INDEX", 1);
define("ACTION_DEFAULT", "index");
define("STATUSID_DEFAULT", 1);
define("TYPE_DEFAULT", "html");
define("COMMON_DEFAULT", "common");

define("DB_HOST", "...");
define("DB_NAME", "...");
define("DB_USER", "...");
define("DB_PASS", "...");
define("DB_PREFIX", "");

define("PAGE_DEFAULT", 1);
define("ROWS_DEFAULT", 1);
define("SIDX_DEFAULT", 1);
define("SORD_DEFAULT", 'ASC');

/**
* Config
*/
class Config  {
}


