<?
namespace Application\Controllers;
	
/**
* Controller
*/
abstract class Controller {

	protected $model;
	protected $view;
	protected $responseData = array();

	function __construct() {
		$this->view = new \Application\Views\View;
		//Толькл view, у наследников свои модели
		//$this->model = ...;
	}

	public function index() {
		if (DEBUG)
			echo __METHOD__."<br />";
	}	
	
	// Тип шаблона (JSON или HTML) - в параметрах
	protected function getTemplateNames($action) {
		$type = \Params::get('type', TYPE_DEFAULT);
		// Общий шаблон
		$commonTemplate = COMMON_DEFAULT."_$type.tpl";
		//Шаблон для конетнта - имяакции_тип.tpl
		$contentTemplate = $action."_$type.tpl";
		
		if (!file_exists(TEMPLATE_PATH.$commonTemplate)) {
			\Error::trow(__METHOD__." Файл шаблона не найден: ".$commonTemplate);
		}
		if (!file_exists(TEMPLATE_PATH.$contentTemplate)) {
			\Error::trow(__METHOD__." Файл шаблона не найден: ".$contentTemplate);
		}
		return array($commonTemplate, $contentTemplate);
	}
	
	// Параметры пагинации для указанного в $count числа записей
	protected function preparePaginationAndHeader($count) {
		$page = intval(\Params::get('page', PAGE_DEFAULT));
		$limit = intval(\Params::get('rows', ROWS_DEFAULT));
		if ($limit == 0) { // Возможно извне пришел 0
			$limit = ROWS_DEFAULT;
		}
		$sidx = intval(\Params::get('sidx', SIDX_DEFAULT));
		if ($sidx == 0) {
			$sidx = 1;
		}
		$sord = \Helper::getSafeString(\Params::get('sord', SORD_DEFAULT));
		if( $count > 0 ) {
		    $total_pages = ceil($count / $limit);
		} else {
		    $total_pages = 0;
		}
		if ($page > $total_pages) 
			$page = $total_pages;
		$start = $limit * $page - $limit; // do not put $limit*($page - 1)
		if ($start < 0) 
			$start = 0;
		// Параметры пагинации для обращения к модели
		$requestData = compact('sidx', 'sord', 'start', 'limit');
		// Параметры пагинации для view
		$header = compact('page', 'total_pages', 'count');
		$data = array($header, $requestData);
		return $data;
	}
	
	// Установка дополнительных заголовков
	protected function setAddedHeaders() {
		$headers = \Framework\Core\Secure::getAddedHeaders();
		foreach ($headers as $header) {
			header($header);
		}
	}
	
	protected function render($name) {
		//Установим дополнительные заголовки
		$this->setAddedHeaders();
		// Получим названия общего и контентного шаблонов для текущего метода (акции)
		list($commonTemplate, $contentTemplate) = $this->getTemplateNames($name);
		$this->view->generate($commonTemplate, $contentTemplate, $this->responseData);		
	}	
	
}