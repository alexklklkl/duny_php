<?
namespace Application\Controllers;
	
/**
* Order Controller
*/
class OrderController extends Controller {
	function __construct() {
		parent::__construct();
		$this->model = new \Application\Models\OrderModel;
	}

	public function orders() {
		$statusId = intval(\Params::get('statusId', STATUSID_DEFAULT));
		
		// Число элементов - нужно для пагинации
		$count = $this->model->getOrderCountWithStatus($statusId);
		// На основе числа элементов сформируем параметры пагинации, в $header будут общие данные для view, в $requestData - данные для модели
		list($commonData, $requestData) = $this->preparePaginationAndHeader($count);
		// В $responseData['header'] - общие данные для view
		$this->responseData['common'] = $commonData;
		// В $responseData['items'] - элементы из модели
		$this->responseData['items'] = $this->model->getOrdersFullTextWithStatus($statusId, $requestData);
		
		// Вывод. Имя контентного шаблона = имя акции (метода)
		$this->render(__FUNCTION__);
	}	
}